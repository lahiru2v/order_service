package com.orderservice.OrderService.controller;

import com.orderservice.OrderService.dto.OrderDTO;
import com.orderservice.OrderService.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    private OrderService orderService;


    @GetMapping("/orderDetails/{id}")
    public List<OrderDTO> getOrderDetailsById(@PathVariable Long id)
    {
        return orderService.getOrderDetails(id);
    }


}
