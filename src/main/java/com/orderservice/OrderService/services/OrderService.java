package com.orderservice.OrderService.services;

import com.orderservice.OrderService.dto.OrderDTO;
import com.orderservice.OrderService.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.jaxb.SpringDataJaxb;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private  final Logger log = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository repo;

    public List<OrderDTO> getOrderDetails(Long id)
    {
        List<OrderDTO> order = null;
        try
        {
            order = repo.findOrderByUserId(id.toString())
                    .stream()
                    .map(orderdata -> new OrderDTO(
                                    orderdata.getId().toString(),
                                    orderdata.getOrderId(),
                                    orderdata.getUserId()
                            )
                    ).collect(Collectors.toList());
        }
        catch (Exception e)
        {
            log.warn(e.getMessage());
        }

        return order;
    }

}
